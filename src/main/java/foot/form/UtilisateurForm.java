package foot.form;


import javax.validation.constraints.NotNull;

public class UtilisateurForm {
    @NotNull
    private String mail;

    @NotNull
    private String password;


    public String getMail(){
        return this.mail;
    }

    public void setMail(String mail){
        this.mail = mail;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }

}
