package foot.form;

import javax.validation.constraints.NotNull;

/**
 * Created by utilisateur on 30/06/2017.
 */
public class InscriptionForm {
    @NotNull
    private String nom;

    @NotNull
    private String mail;

    @NotNull
    private String password;
    @NotNull
    private String passwordConfirm;

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return this.passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}


