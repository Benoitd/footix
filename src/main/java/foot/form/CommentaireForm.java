package foot.form;

import javax.validation.constraints.NotNull;

public class CommentaireForm {

    @NotNull
    private Integer idUtilisateur;

    @NotNull
    private String texte;

    @NotNull
    private Integer idArticle;

    public Integer getIdUtilisateur(){
        return this.idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur){
        this.idUtilisateur = idUtilisateur;
    }

    public String getTexte(){
        return this.texte;
    }

    public void setTexte(String texte){
        this.texte = texte;
    }

    public Integer getIdArticle(){
        return this.idArticle;
    }

    public void setIdArticle(Integer idArticle){
        this.idArticle = idArticle;
    }
}
