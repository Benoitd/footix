package foot.repository;

import foot.entity.ArticleEntity;
import org.springframework.data.repository.CrudRepository;

//@Repository
public interface ArticleRepository extends CrudRepository<ArticleEntity, Integer> {
}
