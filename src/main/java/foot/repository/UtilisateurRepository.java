package foot.repository;

import foot.entity.UtilisateurEntity;

import org.springframework.data.repository.CrudRepository;


public interface UtilisateurRepository extends CrudRepository<UtilisateurEntity, Integer> {

//    UtilisateurEntity findByPsuedo(String psuedo);
}
