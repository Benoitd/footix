package foot.repository;

import foot.entity.CommentaireEntity;
import org.springframework.data.repository.CrudRepository;


public interface CommentaireRepository extends CrudRepository<CommentaireEntity, Integer> {

    Iterable<CommentaireEntity> findByIdArticle(Integer idArticle);
}
