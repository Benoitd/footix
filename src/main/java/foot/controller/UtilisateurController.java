package foot.controller;

import foot.entity.UtilisateurEntity;
import foot.form.InscriptionForm;
import foot.form.LoginForm;
import foot.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;



@Controller
public class UtilisateurController {
    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurController(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    @GetMapping("/inscription")
    public String showForm(InscriptionForm inscriptionForm) {
        return "utilisateur/inscription";
    }

    @PostMapping("/inscription")
    public String checkPersonInfo(@Valid InscriptionForm inscriptionForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "utilisateur/inscription";
        }
        UtilisateurEntity userInscrit = new UtilisateurEntity(
                inscriptionForm.getMail(), inscriptionForm.getPassword(),inscriptionForm.getNom());

        utilisateurRepository.save(userInscrit);

        return "redirect:/";
    }

    @GetMapping("/login")
    public String showForm(LoginForm loginForm) {
        return "utilisateur/login";

    }

    @PostMapping("/login")
    public String checkPersonInfo(@Valid LoginForm loginForm, BindingResult bindingResult, HttpSession session) {

        for ( UtilisateurEntity uttilisateur : utilisateurRepository.findAll()) {
            if(uttilisateur.getEmail().equals(loginForm.getMail())){
                System.out.println("uttilisateur inscrit");
                UtilisateurEntity utilisateurEnregistrer=new UtilisateurEntity();
               utilisateurEnregistrer=uttilisateur;
                if(utilisateurEnregistrer.getMdp().equals(loginForm.getPassword())){
                    // Manipulation du SessionBean
                    return "redirect:/";
                } else {
                    System.out.println("Mdp éronné");
                }
            } else {
                System.out.println("Utilisateur non trouvé, êtes vous inscrit?");
            }
        }





        if (bindingResult.hasErrors()) {
            return "utilisateur/login";
        }

        return "utilisateur/login";
    }



}
