package foot.controller;


//import org.springframework.security.authentication.AnonymousAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
//@EnableJpaRepositories("foot.repository")
public class DefaultController {

    @RequestMapping("/")
    public String home(Model model) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (!(authentication instanceof AnonymousAuthenticationToken)) {
//            String currentUserName = authentication.getName();
//            model.addAttribute("currentUser", currentUserName);
//        }
//        else {
//            model.addAttribute("currentUser",);
//        }
        return "default/home";
    }

    @RequestMapping("/fiche")
    public String ficheInfos() {
        return "default/fiche";
    }

    @RequestMapping("/fiche/{id}")
    public String ficheEquipe() {
        return "default/team";
    }

}

