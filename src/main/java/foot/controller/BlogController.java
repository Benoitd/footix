package foot.controller;

import foot.entity.ArticleEntity;
import foot.entity.CommentaireEntity;
import foot.entity.UtilisateurCommentaire;
import foot.entity.UtilisateurEntity;
import foot.form.CommentaireForm;
import foot.repository.ArticleRepository;
import foot.repository.CommentaireRepository;
import foot.repository.UtilisateurRepository;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BlogController {

    private final ArticleRepository articleRepository;
    private final CommentaireRepository commentaireRepository;
    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    public BlogController(ArticleRepository articleRepository, CommentaireRepository commentaireRepository,
                          UtilisateurRepository utilisateurRepository) {
        this.articleRepository = articleRepository;
        this.commentaireRepository = commentaireRepository;
        this.utilisateurRepository = utilisateurRepository;
    }

    @RequestMapping("/blog")
    public String derniersArticles(Model model) {
//        HttpSession session = request.getSession();
//        UtilisateurEntity user = (UtilisateurEntity) session.getAttribute("user");
//        System.out.println(user);
        model.addAttribute("articles", articleRepository.findAll());
//        model.addAttribute("user", user);
        return "blog/listeArticles";
    }

    @GetMapping("/article/{id}")
    public String article(@PathVariable Integer id, Model model, CommentaireForm commentaireForm) {
        ArticleEntity article = articleRepository.findOne(id);
        Iterable<CommentaireEntity> listCommentaires = commentaireRepository.findByIdArticle(id);
        List<UtilisateurCommentaire> commentaires = new ArrayList<>();
        for (CommentaireEntity commentaire: listCommentaires) {
            UtilisateurCommentaire utilisateurCommentaire = new UtilisateurCommentaire();
            utilisateurCommentaire.setTexte(commentaire.getTexte());
            utilisateurCommentaire.setAuteur(utilisateurRepository.findOne(commentaire.getIdUtilisateur()).getPseudo());
            commentaires.add(utilisateurCommentaire);
        }
        model.addAttribute("article", article);
        model.addAttribute("commentaires", commentaires);
        return "/blog/article";
    }

    @PostMapping("/article/{id}")
    public String saveCommentaire(
                                  @Valid CommentaireForm commentaireForm,
                                  @ModelAttribute CommentaireEntity commentaireEntity,
                                  BindingResult bindingResult
                                  ) {

        if (bindingResult.hasErrors()) {
            System.out.println(commentaireEntity.getIdUtilisateur());
            System.out.println(commentaireEntity.getTexte());
            return "blog/listeArticles";
        }

        CommentaireEntity newCommentaire = new CommentaireEntity();
        newCommentaire.setIdUtilisateur(commentaireEntity.getIdUtilisateur());
        newCommentaire.setTexte(commentaireEntity.getTexte());
        newCommentaire.setIdArticle(commentaireEntity.getIdArticle());
        commentaireRepository.save(newCommentaire);

        return "redirect:/article/"+commentaireEntity.getIdArticle();
    }


}
