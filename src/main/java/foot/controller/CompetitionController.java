package foot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class CompetitionController {

    @RequestMapping("/{id}")
    public String competition() {
        return "competition/ligue";
    }

    @RequestMapping("/{id}/resultats")
    public String resultats() {
        return "/competition/resultats";
    }

    @RequestMapping("/{id}/classement")
    public String classement() {
        return "competition/classement";
    }

//    @GetMapping("/login")
//    public String showForm(LoginForm loginForm) {
//        return "login";
//    }
//
//    @PostMapping("/login")
//    public String checkPersonInfo(@Valid LoginForm loginForm, BindingResult bindingResult) {
//        if (bindingResult.hasErrors()) {
//            return "login";
//        }
//        return "redirect:/";
//    }
}


