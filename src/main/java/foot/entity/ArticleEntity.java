package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Article", schema = "footix", catalog = "")
public class ArticleEntity {
    private int id;
    private String titre;
    private String image;
    private String resumer;
    private String texte;
    private int idUtilisateur;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre", nullable = true, length = 300)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "image", nullable = true, length = 200)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "resumer", nullable = true, length = 50)
    public String getResumer() {
        return resumer;
    }

    public void setResumer(String resumer) {
        this.resumer = resumer;
    }

    @Basic
    @Column(name = "texte", nullable = true, length = -1)
    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    @Basic
    @Column(name = "id_utilisateur", nullable = false)
    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticleEntity that = (ArticleEntity) o;

        if (id != that.id) return false;
        if (idUtilisateur != that.idUtilisateur) return false;
        if (titre != null ? !titre.equals(that.titre) : that.titre != null) return false;
        if (image != null ? !image.equals(that.image) : that.image != null) return false;
        if (resumer != null ? !resumer.equals(that.resumer) : that.resumer != null) return false;
        if (texte != null ? !texte.equals(that.texte) : that.texte != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (titre != null ? titre.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        result = 31 * result + (resumer != null ? resumer.hashCode() : 0);
        result = 31 * result + (texte != null ? texte.hashCode() : 0);
        result = 31 * result + idUtilisateur;
        return result;
    }
}
