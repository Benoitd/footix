package foot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Saison", schema = "footix", catalog = "")
public class SaisonEntity {
    private String saison;

    @Id
    @Column(name = "saison", nullable = false, length = 25)
    public String getSaison() {
        return saison;
    }

    public void setSaison(String saison) {
        this.saison = saison;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SaisonEntity that = (SaisonEntity) o;

        if (saison != null ? !saison.equals(that.saison) : that.saison != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return saison != null ? saison.hashCode() : 0;
    }
}
