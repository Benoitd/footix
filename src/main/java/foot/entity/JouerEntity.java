package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Jouer", schema = "footix", catalog = "")
@IdClass(JouerEntityPK.class)
public class JouerEntity {
    private int id;
    private int idRencontre;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_rencontre", nullable = false)
    public int getIdRencontre() {
        return idRencontre;
    }

    public void setIdRencontre(int idRencontre) {
        this.idRencontre = idRencontre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JouerEntity that = (JouerEntity) o;

        if (id != that.id) return false;
        if (idRencontre != that.idRencontre) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idRencontre;
        return result;
    }
}
