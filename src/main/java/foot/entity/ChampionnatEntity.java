package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Championnat", schema = "footix", catalog = "")
public class ChampionnatEntity {
    private int id;
    private String nom;
    private String code;
    private String codePays;
    private String saison;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom", nullable = true, length = 25)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "code", nullable = true, length = 5)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "code_pays", nullable = true, length = 3)
    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    @Basic
    @Column(name = "saison", nullable = true, length = 25)
    public String getSaison() {
        return saison;
    }

    public void setSaison(String saison) {
        this.saison = saison;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChampionnatEntity that = (ChampionnatEntity) o;

        if (id != that.id) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (codePays != null ? !codePays.equals(that.codePays) : that.codePays != null) return false;
        if (saison != null ? !saison.equals(that.saison) : that.saison != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (codePays != null ? codePays.hashCode() : 0);
        result = 31 * result + (saison != null ? saison.hashCode() : 0);
        return result;
    }
}
