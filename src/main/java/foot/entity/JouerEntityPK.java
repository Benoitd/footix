package foot.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;


public class JouerEntityPK implements Serializable {
    private int id;
    private int idRencontre;

    @Column(name = "id", nullable = false)
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "id_rencontre", nullable = false)
    @Id
    public int getIdRencontre() {
        return idRencontre;
    }

    public void setIdRencontre(int idRencontre) {
        this.idRencontre = idRencontre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JouerEntityPK that = (JouerEntityPK) o;

        if (id != that.id) return false;
        if (idRencontre != that.idRencontre) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idRencontre;
        return result;
    }
}
