package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Rencontre", schema = "footix", catalog = "")
public class RencontreEntity {
    private int id;
    private Integer scoreDom;
    private Integer scoreVis;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "score_dom", nullable = true)
    public Integer getScoreDom() {
        return scoreDom;
    }

    public void setScoreDom(Integer scoreDom) {
        this.scoreDom = scoreDom;
    }

    @Basic
    @Column(name = "score_vis", nullable = true)
    public Integer getScoreVis() {
        return scoreVis;
    }

    public void setScoreVis(Integer scoreVis) {
        this.scoreVis = scoreVis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RencontreEntity that = (RencontreEntity) o;

        if (id != that.id) return false;
        if (scoreDom != null ? !scoreDom.equals(that.scoreDom) : that.scoreDom != null) return false;
        if (scoreVis != null ? !scoreVis.equals(that.scoreVis) : that.scoreVis != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (scoreDom != null ? scoreDom.hashCode() : 0);
        result = 31 * result + (scoreVis != null ? scoreVis.hashCode() : 0);
        return result;
    }
}
