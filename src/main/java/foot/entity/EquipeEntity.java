package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Equipe", schema = "footix", catalog = "")
public class EquipeEntity {
    private int id;
    private String nomLong;
    private String nomCourt;
    private String fondeeEn;
    private String presidents;
    private String entraineur;


    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom_long", nullable = true, length = 250)
    public String getNomLong() {
        return nomLong;
    }

    public void setNomLong(String nomLong) {
        this.nomLong = nomLong;
    }

    @Basic
    @Column(name = "nom_court", nullable = true, length = 25)
    public String getNomCourt() {
        return nomCourt;
    }

    public void setNomCourt(String nomCourt) {
        this.nomCourt = nomCourt;
    }

    @Basic
    @Column(name = "fondee_en", nullable = true, length = 4)
    public String getFondeeEn() {
        return fondeeEn;
    }

    public void setFondeeEn(String fondeeEn) {
        this.fondeeEn = fondeeEn;
    }

    @Basic
    @Column(name = "presidents", nullable = true, length = 25)
    public String getPresidents() {
        return presidents;
    }

    public void setPresidents(String presidents) {
        this.presidents = presidents;
    }

    @Basic
    @Column(name = "entraineur", nullable = true, length = 25)
    public String getEntraineur() {
        return entraineur;
    }

    public void setEntraineur(String entraineur) {
        this.entraineur = entraineur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EquipeEntity that = (EquipeEntity) o;

        if (id != that.id) return false;
        if (nomLong != null ? !nomLong.equals(that.nomLong) : that.nomLong != null) return false;
        if (nomCourt != null ? !nomCourt.equals(that.nomCourt) : that.nomCourt != null) return false;
        if (fondeeEn != null ? !fondeeEn.equals(that.fondeeEn) : that.fondeeEn != null) return false;
        if (presidents != null ? !presidents.equals(that.presidents) : that.presidents != null) return false;
        if (entraineur != null ? !entraineur.equals(that.entraineur) : that.entraineur != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nomLong != null ? nomLong.hashCode() : 0);
        result = 31 * result + (nomCourt != null ? nomCourt.hashCode() : 0);
        result = 31 * result + (fondeeEn != null ? fondeeEn.hashCode() : 0);
        result = 31 * result + (presidents != null ? presidents.hashCode() : 0);
        result = 31 * result + (entraineur != null ? entraineur.hashCode() : 0);
        return result;
    }
}
