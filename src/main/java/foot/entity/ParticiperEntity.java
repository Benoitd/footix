package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Participer", schema = "footix", catalog = "")
@IdClass(ParticiperEntityPK.class)
public class ParticiperEntity {
    private int id;
    private int idChampionnat;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @Column(name = "id_championnat", nullable = false)
    public int getIdChampionnat() {
        return idChampionnat;
    }

    public void setIdChampionnat(int idChampionnat) {
        this.idChampionnat = idChampionnat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParticiperEntity that = (ParticiperEntity) o;

        if (id != that.id) return false;
        if (idChampionnat != that.idChampionnat) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idChampionnat;
        return result;
    }
}
