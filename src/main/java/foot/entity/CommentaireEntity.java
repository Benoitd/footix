package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Commentaire", schema = "footix")
public class CommentaireEntity {
    private int id;
    private String texte;
    private int idArticle;
    private int idUtilisateur;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "texte", nullable = true, length = 5000)
    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    @Basic
    @Column(name = "id_article", nullable = false)
    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Basic
    @Column(name = "id_utilisateur", nullable = false)
    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommentaireEntity that = (CommentaireEntity) o;

        if (id != that.id) return false;
        if (idArticle != that.idArticle) return false;
        if (idUtilisateur != that.idUtilisateur) return false;
        if (texte != null ? !texte.equals(that.texte) : that.texte != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (texte != null ? texte.hashCode() : 0);
        result = 31 * result + idArticle;
        result = 31 * result + idUtilisateur;
        return result;
    }
}
