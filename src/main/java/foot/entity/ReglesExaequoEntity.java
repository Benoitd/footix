package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Regles_exaequo", schema = "footix", catalog = "")
public class ReglesExaequoEntity {
    private String code;
    private String libelle;

    @Id
    @Column(name = "code", nullable = false, length = 5)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "libelle", nullable = true, length = 50)
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReglesExaequoEntity that = (ReglesExaequoEntity) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (libelle != null ? !libelle.equals(that.libelle) : that.libelle != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (libelle != null ? libelle.hashCode() : 0);
        return result;
    }
}
