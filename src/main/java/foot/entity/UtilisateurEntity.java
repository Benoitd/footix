package foot.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "utilisateur", schema = "footix", catalog = "")
public class UtilisateurEntity {
    private String email;
    private String mdp;
    private String nom;
    private String civilite;
    private int id;
    private String prenom;
    private String pseudo;
    private String adresse;
    private String ville;
    private String codePostal;
    private String telephone;
    private Integer numeroAbonnee;
    private Byte newsletter;
    private Date naissance;

    @Basic
    @Column(name = "email", nullable = true, length = 200)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "mdp", nullable = false, length = 20)
    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    @Basic
    @Column(name = "nom", nullable = false, length = 50)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "civilite", nullable = true, length = 4)
    public String getCivilite() {
        return civilite;
    }

    public void setCivilite(String civilite) {
        this.civilite = civilite;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "prenom", nullable = true, length = 50)
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "pseudo", nullable = true, length = 50)
    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @Basic
    @Column(name = "adresse", nullable = true, length = 400)
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "ville", nullable = true, length = 200)
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Basic
    @Column(name = "code_postal", nullable = true, length = 5)
    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    @Basic
    @Column(name = "telephone", nullable = true, length = 10)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "numero_abonnee", nullable = true)
    public Integer getNumeroAbonnee() {
        return numeroAbonnee;
    }

    public void setNumeroAbonnee(Integer numeroAbonnee) {
        this.numeroAbonnee = numeroAbonnee;
    }

    @Basic
    @Column(name = "newsletter", nullable = true)
    public Byte getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(Byte newsletter) {
        this.newsletter = newsletter;
    }

    @Basic
    @Column(name = "naissance", nullable = true)
    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public UtilisateurEntity(String mail,String pwd, String nom){
        setEmail(mail);
        setMdp(pwd);
        setNom(nom);
    }
    public UtilisateurEntity(){}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UtilisateurEntity that = (UtilisateurEntity) o;

        if (id != that.id) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (mdp != null ? !mdp.equals(that.mdp) : that.mdp != null) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (civilite != null ? !civilite.equals(that.civilite) : that.civilite != null) return false;
        if (prenom != null ? !prenom.equals(that.prenom) : that.prenom != null) return false;
        if (pseudo != null ? !pseudo.equals(that.pseudo) : that.pseudo != null) return false;
        if (adresse != null ? !adresse.equals(that.adresse) : that.adresse != null) return false;
        if (ville != null ? !ville.equals(that.ville) : that.ville != null) return false;
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null) return false;
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) return false;
        if (numeroAbonnee != null ? !numeroAbonnee.equals(that.numeroAbonnee) : that.numeroAbonnee != null)
            return false;
        if (newsletter != null ? !newsletter.equals(that.newsletter) : that.newsletter != null) return false;
        if (naissance != null ? !naissance.equals(that.naissance) : that.naissance != null) return false;

        return true;
    }


       @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (mdp != null ? mdp.hashCode() : 0);
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (civilite != null ? civilite.hashCode() : 0);
        result = 31 * result + id;
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        result = 31 * result + (pseudo != null ? pseudo.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (numeroAbonnee != null ? numeroAbonnee.hashCode() : 0);
        result = 31 * result + (newsletter != null ? newsletter.hashCode() : 0);
        result = 31 * result + (naissance != null ? naissance.hashCode() : 0);
        return result;
    }
}
