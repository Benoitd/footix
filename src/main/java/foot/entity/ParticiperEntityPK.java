package foot.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ParticiperEntityPK implements Serializable {
    private int id;
    private int idChampionnat;

    @Column(name = "id", nullable = false)
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "id_championnat", nullable = false)
    @Id
    public int getIdChampionnat() {
        return idChampionnat;
    }

    public void setIdChampionnat(int idChampionnat) {
        this.idChampionnat = idChampionnat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParticiperEntityPK that = (ParticiperEntityPK) o;

        if (id != that.id) return false;
        if (idChampionnat != that.idChampionnat) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idChampionnat;
        return result;
    }
}
