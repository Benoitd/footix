package foot.entity;

import javax.persistence.*;

@Entity
@Table(name = "Stade", schema = "footix", catalog = "")
public class StadeEntity {
    private int id;
    private Integer capacites;
    private String adresse;
    private String nom;
    private String telephone;
    private Integer idEquipe;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "capacites", nullable = true)
    public Integer getCapacites() {
        return capacites;
    }

    public void setCapacites(Integer capacites) {
        this.capacites = capacites;
    }

    @Basic
    @Column(name = "adresse", nullable = true, length = 250)
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "nom", nullable = true, length = 25)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "telephone", nullable = true, length = 10)
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "id_equipe", nullable = true)
    public Integer getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(Integer idEquipe) {
        this.idEquipe = idEquipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StadeEntity that = (StadeEntity) o;

        if (id != that.id) return false;
        if (capacites != null ? !capacites.equals(that.capacites) : that.capacites != null) return false;
        if (adresse != null ? !adresse.equals(that.adresse) : that.adresse != null) return false;
        if (nom != null ? !nom.equals(that.nom) : that.nom != null) return false;
        if (telephone != null ? !telephone.equals(that.telephone) : that.telephone != null) return false;
        if (idEquipe != null ? !idEquipe.equals(that.idEquipe) : that.idEquipe != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (capacites != null ? capacites.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (idEquipe != null ? idEquipe.hashCode() : 0);
        return result;
    }
}
