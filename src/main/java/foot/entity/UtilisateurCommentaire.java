package foot.entity;


public class UtilisateurCommentaire {

    private String texte;
    private String auteur;

    public void setTexte(String texte){
        this.texte = texte;
    }

    public String getTexte(){
        return this.texte;
    }

    public void setAuteur(String auteur){
        this.auteur = auteur;
    }

    public String getAuteur(){
        return  this.auteur;
    }
}
