#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Equipe
#------------------------------------------------------------

CREATE TABLE Equipe(
        id         int (11) Auto_increment  NOT NULL ,
        nom_long    Varchar (250) ,
        nom_court   Varchar (25) ,
        fondee_en    Varchar (4) ,
        presidents Varchar (25) ,
        entraineur Varchar (25) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Championnat
#------------------------------------------------------------

CREATE TABLE Championnat(
        id        int (11) Auto_increment  NOT NULL ,
        nom       Varchar (25) ,
        code      Char (5) ,
        code_pays Char (3) ,
        saison    Varchar (25) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Pays
#------------------------------------------------------------

CREATE TABLE Pays(
        code    Char (3) NOT NULL ,
        libelle Varchar (25) ,
        PRIMARY KEY (code )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Regles_exaequo
#------------------------------------------------------------

CREATE TABLE Regles_exaequo(
        code    Char (5) NOT NULL ,
        libelle Varchar (50) ,
        PRIMARY KEY (code )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Article
#------------------------------------------------------------

CREATE TABLE Article(
        id             int (11) Auto_increment  NOT NULL ,
        titre          Varchar (300) ,
        image          Varchar (200) ,
        resumer        Varchar (50) ,
        texte          Varchar (100000) ,
        id_utilisateur Int NOT NULL,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commentaire
#------------------------------------------------------------

CREATE TABLE Commentaire(
        id             int (11) Auto_increment  NOT NULL ,
        texte          Varchar (5000) ,
        id_article     Int NOT NULL ,
        id_utilisateur Int NOT NULL ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Role
#------------------------------------------------------------

CREATE TABLE Role(
        id      int (11) Auto_increment  NOT NULL ,
        libelle Varchar (25) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Rencontre
#------------------------------------------------------------

CREATE TABLE Rencontre(
        id       int (11) Auto_increment  NOT NULL ,
        score_dom Int ,
        score_vis Int ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
id           int (11) Auto_increment  NOT NULL ,
        email        Varchar (200) ,
        mdp          Varchar (20) NOT NULL ,
        nom          Varchar (50) NOT NULL ,
        prenom       Varchar (50) ,
        pseudo       Varchar (50) ,
        civilite     Varchar (4) ,
        adresse      Varchar (400) ,
        ville        Varchar (200) ,
        code_postal   Varchar (5) ,
        telephone    Varchar (10) ,
        numero_abonnee Int ,
        newsletter   Boolean ,
        naissance    Date ,
        id_role      Int  ,
        code         Char (3) ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Saison
#------------------------------------------------------------

CREATE TABLE Saison(
        saison Varchar (25) NOT NULL ,
        PRIMARY KEY (Saison )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Stade
#------------------------------------------------------------

CREATE TABLE Stade(
        id        int (11) Auto_increment  NOT NULL ,
        nom       Varchar (25) ,
        capacites Int ,
        telephone Varchar (10) ,
        adresse   Varchar (250) ,
        id_equipe Int ,
        PRIMARY KEY (id )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Participer
#------------------------------------------------------------

CREATE TABLE Participer(
        id             Int NOT NULL ,
        id_championnat Int NOT NULL ,
        PRIMARY KEY (id ,id_Championnat )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Jouer
#------------------------------------------------------------

CREATE TABLE Jouer(
        id           Int NOT NULL ,
        id_rencontre Int NOT NULL ,
        PRIMARY KEY (id ,id_Rencontre )
)ENGINE=InnoDB;

ALTER TABLE Championnat ADD CONSTRAINT FK_Championnat_code FOREIGN KEY (code) REFERENCES Regles_exaequo(code);
ALTER TABLE Championnat ADD CONSTRAINT FK_Championnat_code_Pays FOREIGN KEY (code_pays) REFERENCES Pays(code);
ALTER TABLE Championnat ADD CONSTRAINT FK_Championnat_Saison FOREIGN KEY (saison) REFERENCES Saison(saison);
ALTER TABLE Article ADD CONSTRAINT FK_Article_id_Utilisateur FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Commentaire ADD CONSTRAINT FK_Commentaire_id_Article FOREIGN KEY (id_article) REFERENCES Article(id);
ALTER TABLE Commentaire ADD CONSTRAINT FK_Commentaire_id_Utilisateur FOREIGN KEY (id_utilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Utilisateur ADD CONSTRAINT FK_Utilisateur_id_Role FOREIGN KEY (id_role) REFERENCES Role(id);
ALTER TABLE Utilisateur ADD CONSTRAINT FK_Utilisateur_code FOREIGN KEY (code) REFERENCES Pays(code);
ALTER TABLE Stade ADD CONSTRAINT FK_Stade_id_Equipe FOREIGN KEY (id_equipe) REFERENCES Equipe(id);
ALTER TABLE Participer ADD CONSTRAINT FK_Participer_id FOREIGN KEY (id) REFERENCES Equipe(id);
ALTER TABLE Participer ADD CONSTRAINT FK_Participer_id_Championnat FOREIGN KEY (id_championnat) REFERENCES Championnat(id);
ALTER TABLE Jouer ADD CONSTRAINT FK_Jouer_id FOREIGN KEY (id) REFERENCES Equipe(id);
ALTER TABLE Jouer ADD CONSTRAINT FK_Jouer_id_Rencontre FOREIGN KEY (id_rencontre) REFERENCES Rencontre(id);
